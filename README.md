# Demo1
## Integrantes
- Pedro Baez
- Cristobal Barreto 

## Pasos para instalar

1. Clonar el repositorio

    ```bash
    git clone https://gitlab.com/epeat96/demo1.git
    ```

2. Instalar dependencias

    ```bash
    npm install
    ```

## Pasos para levantar

```bash
npm run dev
```

